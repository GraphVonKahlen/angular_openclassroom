import { Component, OnInit } from '@angular/core';
import {AppServices} from '../appServices/app.services';

@Component({
  selector: 'app-element-view',
  templateUrl: './element-view.component.html',
  styleUrls: ['./element-view.component.scss']
})
export class ElementViewComponent implements OnInit {

  isAuth = false;

  lastUpdate = new Promise((resolve, reject) => {
    const date = new Date();
    setTimeout(
        () => {
          resolve(date);
        }, 2000
    );
  });
  devices: any[];

  constructor( private appServices: AppServices) {
    setTimeout(
        () => {
          this.isAuth = true;
        }, 4000
    );
  }

  ngOnInit() {
    this.devices = this.appServices.devices;
  }

  switchOn() {
    this.appServices.turnOnAll();
  }
  switchOff() {
    this.appServices.turnOffAll();
  }

}
