import { Component, OnInit } from '@angular/core';
import {AppServices} from '../appServices/app.services';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-single-element',
  templateUrl: './single-element.component.html',
  styleUrls: ['./single-element.component.scss']
})
export class SingleElementComponent implements OnInit {

  constructor( private appServices: AppServices, private route: ActivatedRoute) { }
  name = 'Device Name';
  status = 'Device Status';

  ngOnInit() {
    const id = this.route.snapshot.params.id;
    this.name = this.appServices.getElementById(+id).name;
    this.status = this.appServices.getElementById(+id).status;
  }

}
