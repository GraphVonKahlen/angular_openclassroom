import {Component, Input, OnInit} from '@angular/core';
import {AppServices} from '../appServices/app.services';

@Component({
  selector: 'app-device',
  templateUrl: './device.component.html',
  styleUrls: ['./device.component.scss']
})
export class DeviceComponent implements OnInit {
  @Input() deviceName: string;
  @Input() deviceStatus: string;
  @Input() deviceIndex: number;
  @Input() id: number;

  constructor(private appServices: AppServices) { }

  ngOnInit() {
  }

  getStatus() {
    return this.deviceStatus;
  }
  getColor() {
    if (this.deviceStatus === 'ON') {
      return 'green';
    } else if (this.deviceStatus === 'OFF') {
      return 'red';
    }
  }
  switchOnOne() {
    this.appServices.switchOnOne(this.deviceIndex);
  }
  switchOnOff() {
    this.appServices.switchOneOff(this.deviceIndex);
  }
}
