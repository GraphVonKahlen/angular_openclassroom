export class AppServices {
    devices = [
        {
            id: 1,
            name: 'Machine à laver',
            status: 'ON'
        },
        {
            id: 2,
            name: 'Frigo',
            status: 'OFF'
        },
        {
            id: 3,
            name: 'Alienware',
            status: 'ON'
        }
    ];

    turnOnAll() {
        for ( const device of this.devices) {
            device.status = 'ON';
        }
    }
    turnOffAll() {
        for (const device of this.devices) {
            device.status = 'OFF';
        }
    }
    switchOnOne(index: number) {
        this.devices[index].status = 'ON';
    }
    switchOneOff(index: number) {
        this.devices[index].status = 'OFF';
    }

    getElementById(id: number) {
        const device = this.devices.find(
            () => {
                return id === id;
            }
        );
        return device;
    }
}
