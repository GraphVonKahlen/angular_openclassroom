import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule} from '@angular/forms';
import { DeviceComponent } from './device/device.component';

import { AppServices} from './appServices/app.services';
import { AuthComponent } from './auth/auth.component';
import { ElementViewComponent } from './element-view/element-view.component';
import {RouterModule, Routes} from '@angular/router';
import {AuthServices} from './appServices/auth.services';
import { SingleElementComponent } from './single-element/single-element.component';
import { FourOhFourComponent } from './four-oh-four/four-oh-four.component';


const appRoutes: Routes = [
  {path: 'devices', component: ElementViewComponent},
  {path: 'devices/:id', component: SingleElementComponent},
  {path: 'auth', component: AuthComponent},
  {path: '', component: ElementViewComponent},
  {path: 'not-found', component: FourOhFourComponent},
  {path: '**', redirectTo: 'not-found'}
];

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    ElementViewComponent,
    SingleElementComponent,
    FourOhFourComponent,
    DeviceComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
      AppServices,
      AuthServices,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
